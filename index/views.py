from datetime import date

from django.views.generic import TemplateView
from django.db.models import Sum, Count, Avg, Max, Min
from django.contrib.auth.models import User

from books.models import Books


class IndexView(TemplateView):
    template_name = "index/index.html"

    def get(self, request, *args, **kwargs):
        books_pages_count = Books.objects.aggregate(
            book_count=Count("id"), sum_pages=Sum("num_pages")
        )
        user_count = User.objects.count()
        user_book_statistics = (
            User.objects.filter(books__date__year=date.today().year)
            .annotate(
                sum_pages=Sum("books__num_pages"),
                books_count=Count("books"),
                avg_pages=Avg("books__num_pages"),
                max_pages=Max("books__num_pages"),
                min_pages=Min("books__num_pages"),
            )
            .order_by("-sum_pages")[:10]
        )

        kwargs.update(
            {
                "book_count": books_pages_count.get("book_count"),
                "page_count": books_pages_count.get("sum_pages"),
                "user_count": user_count,
                "user_book_statistics": user_book_statistics,
            }
        )
        return super().get(request, **kwargs)
