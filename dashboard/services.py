import calendar
from abc import ABC, abstractmethod
from collections import defaultdict

import pygal
from django.contrib.auth.models import User
from pygal.style import NeonStyle, DarkColorizedStyle
from django.utils.translation import gettext as _

from books.models import Books

# params for DarkColorized pie
DarkColorizedStyle.background = "transparent"
DarkColorizedStyle.plot_background = "transparent"
DarkColorizedStyle.title_font_size = 22
DarkColorizedStyle.legend_font_size = 18
DarkColorizedStyle.tooltip_font_size = 16

# params for Neon pie
NeonStyle.background = "transparent"
NeonStyle.plot_background = "transparent"
NeonStyle.title_font_size = 26
NeonStyle.legend_font_size = 18
NeonStyle.tooltip_font_size = 16


class BaseChart(ABC):
    def __init__(self, user, year):
        self.user: User = user
        self.year: str = year

    @staticmethod
    @abstractmethod
    def get_chart_instance():
        pass

    @abstractmethod
    def build_chart(self) -> dict:
        pass

    @property
    def years_of_reading(self) -> list:
        return Books.objects.get_years_of_reading(self.user)

    @staticmethod
    def get_months() -> dict:
        """
        :return: dict str(month name): int(month number)
        """
        return {_(calendar.month_name[i]): i for i in range(1, 13)}

    def get_reading_time(self) -> str:
        return "{}".format(self.year) if self.year.isdigit() else _("all the time")

    def years_to_show(self) -> list:
        """Return all years if user don't ask for specific year or chosen year instead."""
        if self.year.isnumeric():
            return [self.year]
        return self.years_of_reading


class DonutChart(BaseChart):
    @staticmethod
    def get_chart_instance():
        """Create instance and return instance of chart."""
        donut = pygal.Pie(
            inner_radius=0.4, fill=True, interpolate="cubic", style=DarkColorizedStyle
        )
        return donut

    def build_chart(self) -> dict:
        chart = self.get_chart_instance()
        chart.title = _("Type of books for {}").format(self.get_reading_time())
        type_counts = Books.get_type_counts()
        book_types = Books.get_book_type_names()

        for number_of_type, name_of_type in zip(range(1, type_counts + 1), book_types):
            filter_kwargs = {
                "user": self.user,
                "is_active": True,
                "type_of_book": number_of_type,
            }
            if self.year.isdigit():
                filter_kwargs["date__year"] = int(self.year)

            # filtering queries
            count_book_type = Books.objects.filter(**filter_kwargs).count()

            # added type and count to chart
            chart.add(name_of_type, count_book_type)

        return {"donut_chart_type_books": chart.render_data_uri()}


class PieChart(BaseChart):
    @staticmethod
    def get_chart_instance():
        """Create instance and return instance of chart."""
        pie = pygal.Pie(fill=True, interpolate="cubic", style=NeonStyle)
        return pie

    def get_statistics(self) -> dict:
        """
        Construction of statistics by months for books and pages.
        Also counts the books and pages for the year.
        """
        # https://docs.python.org/3/library/collections.html#collections.defaultdict
        books_by_month, pages_by_month = defaultdict(int), defaultdict(int)
        total_books = total_pages = 0
        data = Books.objects.get_books_data(self.user)
        months = self.get_months()

        # fill dicts depending on which exact year was chosen
        for year in self.years_to_show():
            for month in data[year]:
                # count the days when book was read
                total_books += len(data[year][month]["days"])
                # count pages from each month
                total_pages += data[year][month]["num_pages"]

                books_by_month[month] += len(data[year][month]["days"])
                pages_by_month[month] += data[year][month]["num_pages"]

        return {
            "by_month": {
                # sorted by name of month
                # dict_items([('March', 3)])
                "books": sorted(books_by_month.items(), key=lambda k_v: months[k_v[0]]),
                "pages": sorted(pages_by_month.items(), key=lambda k_v: months[k_v[0]]),
            },
            "total": {"books": total_books, "pages": total_pages},
        }

    def build_chart(self):
        pie_chart_books = self.get_chart_instance()
        pie_chart_books.title = _("Read books for {}").format(self.get_reading_time())

        pie_chart_pages = self.get_chart_instance()
        pie_chart_pages.title = _("Read pages for {}").format(self.get_reading_time())

        statistics = self.get_statistics()

        books_by_month = statistics["by_month"]["books"]
        pages_by_month = statistics["by_month"]["pages"]

        # insert into the chart month and counts
        for books_month, pages_month in zip(books_by_month, pages_by_month):
            # month and counts in tuple: ('March', int)
            pie_chart_books.add(books_month[0], books_month[1])
            pie_chart_pages.add(pages_month[0], pages_month[1])

        data_charts_pie = {
            "pie_chart_books": pie_chart_books.render_data_uri(),
            "pie_chart_pages": pie_chart_pages.render_data_uri(),
            "num_books": statistics["total"]["books"],
            "num_pages": statistics["total"]["pages"],
            "all_years": self.years_of_reading,
        }
        return data_charts_pie


class HistogramChart(BaseChart):
    @staticmethod
    def get_chart_instance():
        """Create instance and return instance of chart."""
        histogram = pygal.Line(
            interpolate="cubic",
            height=350,
            tooltip_border_radius=4,
            style=DarkColorizedStyle,
        )
        return histogram

    def get_statistics(self) -> tuple:
        """
        Construction of statistics by months for books and pages.
        """
        # matrix foк months
        line_list_books, line_list_pages = [[0 for _ in range(12)] for _ in range(2)]
        months = self.get_months()
        data = Books.objects.get_books_data(self.user)

        # fill dicts depending on which exact year was chosen
        for year in self.years_to_show():
            for inx, month in enumerate(months):
                if month in data[year]:  # only the month the user read the book
                    line_list_books[inx] += len(data[year][month]["days"])
                    line_list_pages[inx] += data[year][month]["num_pages"]

        return line_list_books, line_list_pages

    def build_chart(self) -> dict:
        line_chart_books = self.get_chart_instance()
        line_chart_books.title = _("Books line charts for {} by month").format(
            self.get_reading_time()
        )
        line_chart_pages = self.get_chart_instance()
        line_chart_pages.title = _("Pages line charts for {} by month").format(
            self.get_reading_time()
        )
        # months in bottom of chart
        line_chart_books.x_labels, line_chart_pages.x_labels = (
            self.get_months(),
            self.get_months(),
        )
        # get statistics for books and pages
        line_list_books, line_list_pages = self.get_statistics()
        # added data to charts
        line_chart_books.add(_("Books"), line_list_books)
        line_chart_pages.add(_("Pages"), line_list_pages)

        return {
            "line_chart_books": line_chart_books.render_data_uri(),
            "line_chart_pages": line_chart_pages.render_data_uri(),
        }
