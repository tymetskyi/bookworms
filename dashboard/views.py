from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView

from books.models import Books
from .services import HistogramChart, DonutChart, PieChart


class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard/dashboard.html"

    def get(self, request, *args, **kwargs):
        user = request.user
        year = request.GET.get("year")
        if not year:
            year = Books.get_max_year_of_book(user)

        histogram = HistogramChart(user=user, year=year)
        donut = DonutChart(user=user, year=year)
        pie = PieChart(user=user, year=year)

        kwargs.update(
            {
                **pie.build_chart(),
                **donut.build_chart(),
                **histogram.build_chart(),
                "current_year": year,
            }
        )

        return super().get(request, **kwargs)
