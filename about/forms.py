from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import Response


class ResponseForm(forms.ModelForm):
    class Meta:
        model = Response
        exclude = ["user"]

        widgets = {
            "message": forms.Textarea(
                attrs={
                    "placeholder": _("Write us a review :)"),
                }
            ),
        }
