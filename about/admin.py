from django.contrib import admin

from .models import Response


@admin.register(Response)
class ResponseAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Response._meta.fields]

    class Meta:
        model = Response
