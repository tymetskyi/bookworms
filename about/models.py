from django.db import models
from django.contrib.auth.models import User


class Response(models.Model):
    # allow anon user create response
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    message = models.TextField(max_length=1024)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return "Response from {}".format(self.user if self.user else "Anonymous")

    class Meta:
        verbose_name = "Response"
        verbose_name_plural = "Responses"
