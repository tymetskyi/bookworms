from django import test
from django.contrib.auth import get_user_model

from about.models import Response

User = get_user_model()


class ResponseTest(test.TestCase):
    """Test Response."""

    @classmethod
    def setUpTestData(cls):
        user = User.objects.create_user(username="test_user", password="12345")
        cls.user_response = Response.objects.create(user=user, message="Test message")
        cls.anonymous_response = Response.objects.create(message="Test message")

    def test_object_str(self):
        expected_with_user = f"Response from {self.user_response.user}"
        self.assertEqual(str(self.user_response), expected_with_user)

        expected_with_anonymous = "Response from Anonymous"
        self.assertEqual(str(self.anonymous_response), expected_with_anonymous)
