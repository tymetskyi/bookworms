from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.utils.translation import ugettext_lazy as _

from .forms import ResponseForm
from .models import Response
from .tasks import send_email_after_feedback, send_email_notification_about_feedback


class AboutView(SuccessMessageMixin, CreateView):
    template_name = "about/about.html"
    model = Response
    form_class = ResponseForm
    success_url = reverse_lazy("about")
    success_message = _("Thanks for response.")

    # def form_valid(self, form):
    #     """
    #     If user is_authenticated save it.
    #     """
    #     email = None
    #
    #     if self.request.user.is_authenticated:
    #         form.instance.user = self.request.user
    #
    #         email = form.instance.user.email
    #         if email is not None:
    #             send_email_after_feedback.delay(
    #                 email, form.instance.user.first_name, form.instance.user.last_name
    #             )
    #
    #     send_email_notification_about_feedback.delay(
    #         user_email=email or "Anonymous", message=form.instance.message
    #     )
    #     return super().form_valid(form)
