from django.conf import settings
from django.contrib.auth.models import User
from django.core.mail import send_mail

from config.celery import app


@app.task
def send_email_after_feedback(user_email, first_name, last_name):
    """Send email to user after he has send a feedback."""
    send_mail(
        subject="Thanks for you response!",
        message=f"Hello, {first_name} {last_name}, our team thanks for your feedback."
        f" We'll be happy to consider your suggestion or comments."
        f" Best regards, BookWorms.",
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[user_email],
        fail_silently=False,
    )


@app.task
def send_email_notification_about_feedback(user_email, message):
    """Send email BookWorms about new feedback."""
    send_mail(
        subject=f"New feedback from {user_email}",
        message=message,
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[settings.EMAIL_HOST_BOOKWORMS],
        fail_silently=False,
    )


@app.task
def send_beat_email():
    """Send monthly user statistics."""
    # todo: monthly user statistics
    for user in User.objects.all():
        if user.email:
            send_mail(
                subject=f"Test spam to {user.username}",
                message="Message",
                from_email=settings.EMAIL_HOST_USER,
                recipient_list=[user.email],
                fail_silently=False,
            )
