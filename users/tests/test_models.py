from django import test
from django.contrib.auth.models import User

from users.models import Profile


class ProfileTestCase(test.TestCase):
    """Test Profile."""

    @classmethod
    def setUpTestData(cls):
        user = User.objects.create_user("Someone", "test@gmail.com", "11111111")
        cls.profile = Profile(user=user)

    def test_book_str(self):
        expected = self.profile.user.username
        self.assertEqual(str(self.profile), expected)
