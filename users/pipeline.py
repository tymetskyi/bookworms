from social_core.backends.google import GoogleOAuth2
from social_core.backends.facebook import FacebookOAuth2

from .models import Profile


def save_avatar(backend, user, response, *args, **kwargs):
    """
    Get or create (because this field is unique)
    profile object after save url to avatar.
    """
    url = ""

    if isinstance(backend, GoogleOAuth2):
        url = response.get("picture")
    elif isinstance(backend, FacebookOAuth2):
        url = response.get("picture")["data"]["url"]

    if url:
        Profile.objects.get_or_create(user=user)
        user.profile.avatar = url
        user.profile.save()
