from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    """User model extension."""

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.URLField()

    def __str__(self):
        return self.user.username
