var jetBubbles = document.getElementsByClassName('jetBubble');
var rocketManSVG = document.querySelector('.rocketManSVG');
var star = document.querySelector('.star');
var satellite = document.querySelector('.satellite');
var astronaut = document.querySelector('.astronaut');

TweenMax.to(astronaut, .6, {
    y: '+=12',
    repeat: -1,
    yoyo: true
});

var mainTimeline = new TimelineMax({repeat: -1});

mainTimeline.timeScale(6).seek(100);

function createJets() {
    TweenMax.set(jetBubbles, {
        attr: {
            r: '-=5'
        }
    });
    //jet bubbles
    for (var i = 0; i < jetBubbles.length; i++) {
        var jb = jetBubbles[i];
        var tl = new TimelineMax({repeat: -1});
        tl.to(jb, 2, {
            attr: {
                r: '+=15'
            },
            ease: Linear.easeNone
        })
        .to(jb, 2, {
            attr: {
                r: '-=15'
            },
            ease: Linear.easeNone
        });

        mainTimeline.add(tl, i / 4)
    }

    rocketManSVG.removeChild(star);
}


var satTimeline = new TimelineMax({repeat: -1});
satTimeline.to(satellite, 46, {
    rotation: 360,
    transformOrigin: '50% 50%',
    ease: Linear.easeNone
});


TweenMax.staggerTo('.pulse', 0.8, {
    alpha: 0,
    repeat: -1,
    ease: Power2.easeInOut,
    yoyo: false
}, 0.1);


TweenMax.staggerTo('.satellitePulse', 0.8, {
    alpha: 0,
    repeat: -1,
    ease: Power2.easeInOut,
    yoyo: false
}, 0.1);

createJets();
