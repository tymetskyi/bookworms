var landing = {

    scrollTo: function () {
      $('.scrollTo').click(function (event) {
        event.preventDefault();
        var id = $(this).attr("data-target"),
          top = $(id).offset().top;
        $('html, body').animate({scrollTop: top}, 800, "linear");
      });
    },

    upDownSlider: function () {
        //sl2
        let interval;
        $(document).ready(function() {
          var nextBtn = $('.sectionPostsCarouselSpecificControls .nav .btn-next');
          var prevBtn = $('.sectionPostsCarouselSpecificControls .nav .btn-prew');
          var heightSlide = 100;
          var maxSlides = 3;
          var arr = ['What is BookWorms?', 'What about time management?',
            'Pluses of readings', 'EBook or Printed Book'];
          var inx = 0;
          var numImg = 1;

          nextBtn.on('click', function() {
            clearInterval(interval);
            prevBtn.removeClass('disabled');

            //text
            $('.info-specific').fadeOut('fast', function() {
              inx += 1;
              $(this).find('.project').html(arr[inx]);
            });
            $('.info-specific').fadeIn('fast', function() {});

            //btn+img
            if(numImg == maxSlides){
              $(this).addClass('disabled');
              $('.sectionPostsCarouselSpecificOld__el[data-img-two='+numImg+']').css({'transform':'translateY(-'+heightSlide+'%)'+'matrix(1, 0, 0, 1, 0, 0)'});
              $('.sectionPostsCarouselSpecificNew__el[data-img-two='+numImg+']').css({'transform':'translateY('+heightSlide+'%)'+'matrix(1, 0, 0, 1, 0, 0)'});
              numImg += 1;
            } else{
              $('.sectionPostsCarouselSpecificOld__el[data-img-two='+numImg+']').css({'transform':'translateY(-'+heightSlide+'%)'+'matrix(1, 0, 0, 1, 0, 0)'});
              $('.sectionPostsCarouselSpecificNew__el[data-img-two='+numImg+']').css({'transform':'translateY('+heightSlide+'%)'+'matrix(1, 0, 0, 1, 0, 0)'});
              numImg += 1;
            }
            autoSwipe();
          });

          prevBtn.on('click', function() {
            clearInterval(interval);
            nextBtn.removeClass('disabled');
            //text
            $('.info-specific').fadeOut('fast', function() {
              inx -= 1;
              $(this).find('.project').html(arr[inx]);
            });
            $('.info-specific').fadeIn('fast', function() {});
            //btn+img
              numImg-=1
              if (numImg == 1){
                $(this).addClass('disabled');
                $('.sectionPostsCarouselSpecificOld__el[data-img-two='+numImg+']').css({'transform':'translateY(0%)'+'matrix(1, 0, 0, 1, 0, 0)'});
                $('.sectionPostsCarouselSpecificNew__el[data-img-two='+numImg+']').css({'transform':'translateY(0%)'+'matrix(1, 0, 0, 1, 0, 0)'});
              } else{
              $('.sectionPostsCarouselSpecificOld__el[data-img-two='+numImg+']').css({'transform':'translateY(0%)'+'matrix(1, 0, 0, 1, 0, 0)'});
              $('.sectionPostsCarouselSpecificNew__el[data-img-two='+numImg+']').css({'transform':'translateY(0%)'+'matrix(1, 0, 0, 1, 0, 0)'});
            }
            autoSwipe();
          });

          function autoSwipe(){
            interval = setInterval(() => {
              if (numImg == 1){
                prevBtn.removeClass('disabled');
              }
              if (numImg == 3){
                nextBtn.addClass('disabled');
              }
              if (numImg == 4) {
                nextBtn.removeClass('disabled');
              }

                if(numImg !== 4){
                  nextBtn.click();
                } else{
                  inx = 0;
                  while(numImg != 1){
                    numImg -= 1;
                    $('.sectionPostsCarouselSpecificOld__el[data-img-two='+numImg+']').css({'transform':'translateY(0%)'+'matrix(1, 0, 0, 1, 0, 0)'});
                    $('.sectionPostsCarouselSpecificNew__el[data-img-two='+numImg+']').css({'transform':'translateY(0%)'+'matrix(1, 0, 0, 1, 0, 0)'});
                  }
                  prevBtn.addClass('disabled');
                  $('.info-specific').fadeOut('fast', function() {
                  $(this).find('.project').html(arr[inx]);
                });
                $('.info-specific').fadeIn('fast', function() {});
                }
              },6000);
          }
          autoSwipe();

        });
    }
};


$(document).ready(function () {
    landing.scrollTo();
    landing.upDownSlider();
});
