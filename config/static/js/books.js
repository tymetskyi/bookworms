var books = {

  changeMobileCalendar: function() {
    let boolDesktop = window.matchMedia('(min-width: 1023px)').matches;
    if (boolDesktop) {
      $('[data-toggle="datepicker"]').datepicker({
        format: 'yyyy-mm-dd'
      });
    } else {
      $('[data-toggle="datepicker"]').attr('type', 'date');
    }
  },

  addBookNextStep: function() {

    $('#next-step-btn').click(function () {
      let flag = true;

      $('.booksListRowAddStep.one input').each(function () {
        if ($(this).val()){
          $(this).removeClass('error-b');
        } else {
          flag = false;
          if (!($(this).val())) {
            $(this).addClass('error-b');
          } else {
            $(this).removeClass('error-b');
          }
        }
      });

      if (flag) {
        $('.one, .two').addClass('next');
        $('.booksListRowAdd').addClass('hidden');
      }

    });

    $('#back-step-btn').click(function () {
      $('.one, .two').removeClass('next');
      setTimeout(function () {
        $('.booksListRowAdd').removeClass('hidden');
      }, 1000);
    });
  },

  openComment: function () {
    $('.comment').on('click', function () {
      $(this).parents('.booksListRowShow').find('.booksListRowShowComment').toggleClass('open');
    });
  },
};

$(document).ready(function () {
  books.changeMobileCalendar();
  books.addBookNextStep();
  books.openComment();
  index.sweetAlertInit(".btn-delete", gettext("delete"),
    'red', '#8813a3', gettext('Delete'));
});
