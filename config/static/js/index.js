var index = {

    activeHeader: function () {
        $(window).scroll(function () {
            var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
            var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
            var scrolled = (winScroll / height) * 100;
            $('header .headerScrollLine span').css('width', scrolled + '%');

            if ($(window).scrollTop() > 0) {
                $('header').addClass('active');
            } else {
                $('header').removeClass('active');
            }
        });
    },

    burgerHeaderFonHider: function () {
        $('#menuToggle input').change(function () {
            $(this).each(function () {
                $('header').toggleClass('active');
            });
        });
    },

    siteLoader: function () {
        $('.loader-cont').addClass('hide');
    },

    sweetAlertInit: function (class_link, title, color1, color2, text_true) {
        $(document).on('click', class_link, function (event) {
            event.preventDefault();
            let redirect_url = $(this).attr('href');

            Swal.fire({
              title: gettext('Are you sure you want to ') + title + '?',
              showCancelButton: true,
              confirmButtonColor: color1,
              cancelButtonColor: color2,
              confirmButtonText:  text_true,
              cancelButtonText:  gettext("No"),
            }).then((result) => {
              if (result.value) {
                window.location.href = redirect_url;
              }
            });
        });
    },

    siteAlert: function () {
        $('.alert').addClass('off');
    },

    logoAnimate: function () {
        const colors = ['#8672e1','pink','#FDCA6E'];
        var blobs = document.querySelectorAll("#background path");

          blobs.forEach(blob => {
              blob.style.fill = colors[Math.floor(Math.random() * colors.length)];
          });
    }

};


$(document).ready(function () {
    setTimeout(index.siteLoader, 1000);
    setTimeout(index.siteAlert, 4000);
    index.activeHeader();
    index.burgerHeaderFonHider();
    index.sweetAlertInit(".headerLogout", gettext("logout"),
      'green', '#8813a3', gettext("Yes" ));
    $('select').niceSelect();
    index.logoAnimate();
});
