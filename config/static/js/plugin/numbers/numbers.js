function count_numbers_init(class_name, number, duration=2600) {
  $(class_name).animateNumber(
    {
      number: number,
      numberStep: function (now, tween) {
          let floored_number = Math.floor(now),
              target = $(tween.elem);
          target.text(floored_number);
      }
    },
    {
      easing: 'swing',
      duration: duration
    }
  );
}