Particles.init({
    selector: '.background',
    maxParticles: 130,
    sizeVariations: 3,
    color: ['#DA0463', '#19035d', '#f2f3c9'],
    // color: ['#1A7F11', '#FFDD00', '#FC6B1E'],
});