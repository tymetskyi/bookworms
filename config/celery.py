import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.production")

app = Celery("BookWorms")
# connect celery settings from our settings file with start name CELERY
app.config_from_object("django.conf:settings", namespace="CELERY")
# auto connect our tasks from apps
app.autodiscover_tasks()

# run tasks on cron
app.conf.beat_schedule = {
    # default name
    "send-message-every-5-minutes": {
        # app.task.task_name
        "task": "about.tasks.send_beat_email",
        "schedule": crontab(minute="*/5"),
    }
}
