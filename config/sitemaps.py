from django.contrib.sitemaps import Sitemap
from django.urls import reverse

# from books.models import Books


# class BlogSitemap(Sitemap):
#     """ sitemap for dynamic pages """
#
#     def items(self):
#         return Books.objects.all()


class StaticSitemap(Sitemap):
    """Sitemap for static pages"""

    priority = 0.85
    changefreq = "never"

    def items(self):
        return ["signup", "login", "logout", "index", "books", "dashboard", "about"]

    def location(self, item):
        return reverse(item)
