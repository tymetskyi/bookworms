# -*- coding: utf-8 -*-
"""BookWorms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.conf.urls.i18n import i18n_patterns
from django.views.generic import TemplateView
from django.views.i18n import JavaScriptCatalog

from django.conf.urls import url
from django.urls import path, include

from about.views import AboutView
from index.views import IndexView

from config.sitemaps import StaticSitemap

sitemaps = {"static": StaticSitemap}
js_info_dict = {"packages": ("languages",)}

clean_url = [
    path("i18n/", include("django.conf.urls.i18n")),  # for change language
    url(r"^jsi18n/", JavaScriptCatalog.as_view(), name="javascript-catalog"),
    path("admin/", admin.site.urls),
    path(
        "sitemap.xml/",
        sitemap,
        {"sitemaps": sitemaps},
        name="django.contrib.sitemaps.views.sitemap",
    ),
    path(
        "robots.txt",
        TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
    ),
    path("accounts/", include("accounts.urls")),
    path("accounts/", include("django.contrib.auth.urls")),
    path("", IndexView.as_view(), name="index"),
]

i18n = i18n_patterns(
    path("", IndexView.as_view(), name="index"),
    path("oauth/", include("social_django.urls", namespace="social")),
    path("accounts/", include("accounts.urls")),
    path("accounts/", include("django.contrib.auth.urls")),
    path("books/", include("books.urls")),
    path("dashboard/", include("dashboard.urls")),
    path("about/", AboutView.as_view(), name="about"),
)

urlpatterns = [*clean_url] + i18n

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
