from django.urls import reverse
from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import CreateView


class SignUpView(CreateView):
    form_class = UserCreationForm
    template_name = "registration/signup.html"

    def form_valid(self, form):
        """Login user directly after registration"""
        valid = super().form_valid(form)
        # login user (default method as ModelBackend) after sign up
        login(
            self.request,
            self.object,
            backend="django.contrib.auth.backends.ModelBackend",
        )
        return valid

    def get_success_url(self):
        return reverse("books")
