from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as MyUserAdmin
from django.contrib.auth.models import User

admin.site.unregister(User)

MyUserAdmin.list_display = (
    "id",
    "username",
    "email",
    "first_name",
    "last_name",
    "is_active",
    "date_joined",
    "is_staff",
)
MyUserAdmin.ordering = ("-date_joined",)
admin.site.register(User, MyUserAdmin)
