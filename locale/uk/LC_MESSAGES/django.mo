��    g      T  �   �      �  #   �  a   �     7	     @	     I	  ,   V	     �	  	   �	     �	     �	  
   �	  �   �	  	   G
     Q
     Z
     u
  
   �
     �
     �
     �
     �
     �
     �
  !   �
     �
  I   �
  (   H  	   q     {     �  ,   �     �     �     �     �     �  	   �                    $     9     >     E  
   K     V     ]     z     �  
   �     �     �     �     �     �     �     �  !   �       
             !     7     P     U     Z  
   a     l     ~     �     �     �     �     �     �     �  
   �     �     �                    -     B     R  	   ^     h     n  !   �     �     �     �      �        #   8     \     |  
   �     �  
   �     �  
   �     �  L   �  �   )            )   !  >   K     �     �     �  "   �     �  �        �       0     $   E     j  
   �     �     �  
   �  #   �     �  B   �  0   ,  �   ]  F   �     D     Y     b  F   s     �     �  +   �          ;     P     g  
   �     �  $   �     �     �     �     �  
     8        P     b     j     v  5        �     �     �     �       J        _     p     �  2   �  2   �       
          #   ,  #   P  +   t  6   �     �     �     �               2     G  7   \  !   �     �     �     �     �  -     2   ?     r       P   �  7   �       '   1  9   Y  ;   �  =   �  *     0   8     i     y     �     �     �     �         <   Y   E   A   P       [      K      O          5       T      .              e       C   `                  Q       L      !   "          ,   I                     \   $      1   %       b   3       a   J   &       (   ?   F   S   0   W          7      B   f       +                    _      	      R   @      4   6       )   d       
   ]       G          /           :   *   '          ;   H   9   Z   c      N           >       #      D      8          g       X      -   ^   2   =   V          M      U    3 Steps For Perfect Book Statistics A place to watch your book progress, make your own must read list or take participation in fierce About Us About us Active Users Actually you aren't having book right now :( Add Add Books Add book Add your books Added Book After spending a long time searching for a resource for statistics to read and not finding what it would berather, came up with the idea to create All books All time And U like statistics too? Annual and quarterly Audio Book Author Author* Average Back Book promotion Books Books line charts for {} by month Check your statistics Creating a comfortable and easy-to-use resource that tracks your progress Creating a friendly community of readers Dashboard Date Delete Do you want remember all of your read books? Don't have an account? Donate Donate us a cup of coffee Ebook Edit Excellent Future plans Good Have an account? Hi, do U love books? Home Log in Login Login with Logout Maybe few of interest posts? My Books Name Name Book* Next One ease-step of registration Our Thoughts Our goal Our history Page  Pages Pages line charts for {} by month Pages* Paper Book Personal Personal rewards page Please check your year:  Poor Prev Rating Read Pages Read books for {} Read pages for {} Register your account Review Save Send Sign up Sing Up Sing up Statistics Statistics all of our users Thanks for response. Theme Total books:  Total pages:  Type of books for {} User Statistics User rating Very Good Watch Watch your statistics and enjoy We will calculate your statistics What is BookWorms Write us a review :) You book was successful added. You book was successful changed. You book was successful deleted. You have come to the right place ;) Your opinion is important to us all the time first step list not found. second step third step Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);
 3 Кроки До Ідеальної Книжкової Статистики Місце, де можна спостерігати свій книжковий прогрес, складати власний must read список або ж братиучасть у запеклих read challenge Про Нас Про Нас Активних користувачів Насправді зараз у вас немає книг :( Додати Додати книги Додати книгу Додайте свої книги Доданих Книг Провівши довгий час у пошуках ресурсу для статистики прочитаного і не знайшовши того, що було б до вподоби, прийшов задум створити  Всі книги Весь час І схиблений на статистиці? Річні та квартальні Аудіо Книга Автор Автор* Непогано Назад Популяризація книг Мої книги Лінійний графік книг за {} по місяцях Перевірте свою статистику Створення комфортного та легкого у використанні ресурсу, котрий відстежує ваш прогрес Створення дружнього ком'юніті читачів Статистика Дата Видалити Хочете пам’ятати всі прочитані книги? Не маєте акаунту? Подякувати Подякувати розробникам Електронна Книга Редагувати Надзвичайно Майбутні плани Добре Маєте акаунт? Хей, обожнюєш книги? Головна Вхід Вхід Вхід з допомогою Вихід Можливо кілька цікавих постів? Мої книги Ім'я Книга* Далі Один простий крок реєстрації Наші Думки Наша мета Наша історія Cторінка  Cторінки Лінійний графік сторінок за {} по місяцях Cторінки* Паперова Книга Особистий Сторінка особистих нагород Будь ласка оберіть ваш рік:  Погано Назад Рейтинг Прочитано сторінок Прочитано книг за {} Прочитано сторінок за {} Зареєструйте обліковий запис Відгук Зберегти Надіслати Реєстрація Реєстрація Реєстрація Статистика Статистика користувачів BookWorms Дякуємо за відгук. Тема Всього книг:  Всього сторінок:  Типи книг за {} Статистика Користувачів Рейтинг серед користувачів Чудово Дивитись Дивіться свою статистику та насолоджуйтесь Ми підрахуємо вашу статистику Що таке BookWorms Напишіть нам фідбек :) Ваша книга була успішно додана. Ваша книга була успішно змінена. Ваша книга була успішно видалена. Ти прийшов за адресою ;) Ваша думка важлива для нас весь час перший крок список не знайдена. другий крок третій крок 