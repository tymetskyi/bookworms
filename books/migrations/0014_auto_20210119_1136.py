# Generated by Django 3.1 on 2021-01-19 11:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0013_auto_20210113_1259'),
    ]

    operations = [
        migrations.RenameField(
            model_name='books',
            old_name='is_favorite',
            new_name='favorite_rank',
        ),
    ]
