# Generated by Django 2.2.7 on 2020-05-18 10:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0009_book_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='type_of_book',
            field=models.IntegerField(choices=[(1, 'PAPER BOOK'), (2, 'Ebook'), (3, 'Audio Book')], default=1),
        ),
    ]
