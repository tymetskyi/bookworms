# Generated by Django 2.2.7 on 2021-01-12 23:15

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0011_auto_20200518_1104'),
    ]

    operations = [
        migrations.RenameField(
            model_name='book',
            old_name='created',
            new_name='created_at',
        ),
        migrations.RenameField(
            model_name='book',
            old_name='update',
            new_name='update_at',
        ),
        migrations.AlterField(
            model_name='book',
            name='author',
            field=models.CharField(max_length=256),
        ),
        migrations.AlterField(
            model_name='book',
            name='name',
            field=models.CharField(max_length=256),
        ),
        migrations.AlterField(
            model_name='book',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
