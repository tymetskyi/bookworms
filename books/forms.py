from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import Books


class BookForms(forms.ModelForm):
    class Meta:
        model = Books
        exclude = ["user", "is_active"]

        widgets = {
            "name": forms.TextInput(
                attrs={
                    "placeholder": _("Name Book*"),
                }
            ),
            "author": forms.TextInput(
                attrs={
                    "placeholder": _("Author*"),
                }
            ),
            "date": forms.DateInput(
                attrs={
                    "data-toggle": "datepicker",
                }
            ),
            "review": forms.Textarea(
                attrs={
                    "rows": "8",
                    "placeholder": _("Review"),
                }
            ),
        }

    def clean_author(self):
        data = self.cleaned_data["author"]
        data = data[0].upper() + data[1:]
        return data

    def clean_name(self):
        data = self.cleaned_data["name"]
        data = data[0].upper() + data[1:]
        return data
