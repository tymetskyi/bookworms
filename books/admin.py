from django.contrib import admin

from .models import Books


@admin.register(Books)
class BookAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Books._meta.fields]
    search_fields = ("name", "author")

    class Meta:
        model = Books
