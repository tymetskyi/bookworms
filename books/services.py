from books.models import Books


def get_book_list_rating_by_user(user) -> dict:
    """
    Building ratting for book.
    :return: dict{'name_book':[list of max numbers 1, 2, 3, 4]}
    """
    books = Books.objects.filter(user=user, is_active=True)
    return {
        book.name: [inx for inx in range(1, book.favorite_rank + 1)] for book in books
    }
