from .models import Books


def getting_books(request):
    books = None

    if request.user.is_authenticated:
        books = Books.objects.filter(user=request.user)

    return {"books": books}
