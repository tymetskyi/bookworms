import calendar
import datetime

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


class BooksManager(models.Manager):
    @staticmethod
    def get_books_data(user) -> dict:
        """
        Build dict object with year, month and day info.
        :return: dict(int: dict(int, dict(int: list(int, int)))
        {'2021': {'January': {'days': ['10', '13', '13', '17'], 'num_pages': 48}}
        """
        books = Books.objects.filter(user=user)
        data = {}

        for book in books:  # create dict year: month: day
            date = str(book.date).split("-")  # year, month, date
            date[1] = str(_(calendar.month_name[int(date[1])]))  # 10 -> May
            num_pages = int(book.num_pages)

            if date[0] in data:
                if date[1] in data[date[0]]:  # month in data[year]
                    data[date[0]][date[1]]["days"].append(date[2])
                    data[date[0]][date[1]]["num_pages"] += num_pages
                else:  # month not in data[year]
                    data[date[0]].update(
                        {date[1]: {"days": [date[2]], "num_pages": num_pages}}
                    )
            else:  # year not in data
                data[date[0]] = {date[1]: {"days": [date[2]], "num_pages": num_pages}}

        return data

    @staticmethod
    def get_years_of_reading(user: User) -> list:
        """
        :return: Sorted list that contains years from the
         top level of the expression.
        """
        books_date = Books.objects.filter(user=user).values_list("date", flat=True)
        years = {str(date.year) for date in books_date}
        return sorted(years)


class Books(models.Model):
    POOR = 1
    AVERAGE = 2
    GOOD = 3
    VERY_GOOD = 4
    EXCELLENT = 5

    PAPER = 1
    E_BOOK = 2
    AUDIO = 3

    Books_type_CHOISES = (
        (PAPER, _("Paper Book")),
        (E_BOOK, _("Ebook")),
        (AUDIO, _("Audio Book")),
    )

    Rating_CHOICES = (
        (POOR, _("Poor")),
        (AVERAGE, _("Average")),
        (GOOD, _("Good")),
        (VERY_GOOD, _("Very Good")),
        (EXCELLENT, _("Excellent")),
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    author = models.CharField(max_length=256)
    num_pages = models.IntegerField(default=0)
    date = models.DateField(default=datetime.date.today)
    type_of_book = models.IntegerField(choices=Books_type_CHOISES, default=1)
    favorite_rank = models.IntegerField(choices=Rating_CHOICES, default=5)
    review = models.TextField(blank=True)

    is_active = models.BooleanField(default=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    update_at = models.DateTimeField(auto_now_add=False, auto_now=True)

    objects = BooksManager()

    def __str__(self):
        return "{}, {}".format(self.name, self.author)

    class Meta:
        verbose_name = "Book"
        verbose_name_plural = "Books"

    def get_list_rating(self):
        """
        Building ratting for book.
        :return: dict{'name_book':[list of max numbers 1, 2, 3, 4]}
        """
        books = Books.objects.filter(user=self.user, is_active=self.is_active)
        return {
            book.name: [inx for inx in range(1, book.favorite_rank + 1)]
            for book in books
        }

    @staticmethod
    def get_max_year_of_book(user) -> str:
        """
        Return last year when the book was created or "all".
        """
        books = Books.objects.filter(user=user, is_active=True)
        max_year = "all"
        if books.exists():
            max_year = str(books.latest("date").date.year)
        return max_year

    @staticmethod
    def get_book_type_names() -> list:
        book_types = Books.Books_type_CHOISES
        name_types = [str(i[1]) for i in book_types]
        return name_types

    @staticmethod
    def get_type_counts() -> int:
        book_types = Books.Books_type_CHOISES
        return len(book_types)
