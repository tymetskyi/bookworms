from django import test
from django.contrib.auth.models import User

from books.models import Books


class BookTestCase(test.TestCase):
    """Test Book."""

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user("Someone", "test@gmail.com", "11111111")
        cls.second_user = User.objects.create_user(
            "Someone Else", "test@gmail.com", "2222222"
        )
        cls.min_date = "2020-12-12"
        cls.max_date = "2022-12-12"
        cls.book = Books.objects.create(
            user=cls.user,
            name="It",
            author="Stephen King",
            num_pages=1344,
            date=cls.min_date,
            type_of_book=Books.PAPER,
        )
        cls.second_book = Books.objects.create(
            user=cls.user,
            name="It",
            author="Stephen King",
            num_pages=1344,
            date=cls.max_date,
        )

    def test_book_str(self):
        expected = "{}, {}".format(self.book.name, self.book.author)
        self.assertEqual(str(self.book), expected)

    def test_name_author_max_length(self):
        name_max_length = self.book._meta.get_field("name").max_length
        author_max_length = self.book._meta.get_field("author").max_length

        for field in name_max_length, author_max_length:
            with self.subTest(n=field):
                self.assertEqual(field, 256)

    def test_book_type_names(self):
        book_types = self.book.Books_type_CHOISES
        name_types = [str(i[1]) for i in book_types]
        self.assertEqual(self.book.get_book_type_names(), name_types)

    def test_books_types_count(self):
        book_types = self.book.Books_type_CHOISES
        self.assertEqual(self.book.get_type_counts(), len(book_types))

    def test_years_of_reading(self):
        data = Books.objects.get_books_data(self.user)
        years_of_reading = Books.objects.get_years_of_reading(self.user)
        expected = list(sorted(data))
        self.assertListEqual(years_of_reading, expected)

    def test_max_year_of_book(self):
        # tested case between 2020 and 2022 years
        max_year = self.book.get_max_year_of_book(self.user)
        self.assertEqual(max_year, self.max_date.split("-")[0])
        # tested case when user has no any books
        max_year = self.book.get_max_year_of_book(self.second_user)
        self.assertEqual(max_year, "all")
