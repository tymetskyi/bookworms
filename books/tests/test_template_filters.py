from django import test

from books.templatetags.template_filters import get_item, is_numeric


class TestIsNumeric(test.TestCase):
    def test_is_numeric_true(self):
        self.assertTrue(is_numeric("2"))

    def test_is_numeric_false(self):
        for not_number in "4r", "":
            with self.subTest(i=not_number):
                self.assertFalse(is_numeric(not_number))


class TestGetItem(test.TestCase):
    test_dict = {1: "3", 4: [3, 4], ("h",): 2}

    def test_get_item_positive(self):
        for key, value in self.test_dict.items():
            with self.subTest(i=key):
                result = get_item(self.test_dict, key)
                self.assertEqual(value, result)

    def test_get_item_negative(self):
        self.assertIsNone(get_item(self.test_dict, 29))
