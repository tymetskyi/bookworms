from django import test
from django.contrib.auth.models import User

from books.forms import BookForms
from books.models import Books


class BookFormsTest(test.TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user("Someone", "test@gmail.com", "11111111")
        cls.name = "it"
        cls.author = "stephen King"
        cls.form = BookForms(
            data={
                "user": cls.user,
                "name": cls.name,
                "author": cls.author,
                "num_pages": 1344,
                "date": "2022-12-12",
                "type_of_book": Books.PAPER,
                "favorite_rank": Books.AVERAGE,
            }
        )

    def test_book_form_is_valid(self):
        self.assertTrue(self.form.is_valid())

    def test_clean_author(self):
        expected = self.name[0].upper() + self.name[1:]
        self.assertEqual(self.form.cleaned_data["name"], expected)

    def test_clean_name(self):
        expected = self.author[0].upper() + self.author[1:]
        self.assertEqual(self.form.cleaned_data["author"], expected)
