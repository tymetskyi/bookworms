from typing import Any, Hashable

from django.template.defaulttags import register


@register.filter
def get_item(dictionary: dict, key: Hashable) -> Any:
    """Return value by key from dict."""
    return dictionary.get(key)


@register.filter(is_safe=True)
def is_numeric(value: str) -> bool:
    return value.isdigit()
