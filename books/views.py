from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import TemplateView, DeleteView, UpdateView
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.utils.translation import ugettext_lazy as _

from .forms import BookForms
from .models import Books
from .services import get_book_list_rating_by_user


class BookView(LoginRequiredMixin, TemplateView):
    template_name = "books/books.html"
    model_form = BookForms

    def get_form(self):
        return self.model_form(data=self.request.POST or None)

    def get_books(self, year, order_by):
        filter_kwargs = {
            "user": self.request.user,
            "is_active": True,
        }
        if year.isnumeric():
            filter_kwargs["date__year"] = int(year)

        return Books.objects.filter(**filter_kwargs).order_by(order_by)

    def get(self, request, *args, **kwargs):
        user = request.user
        max_year = Books.get_max_year_of_book(user)
        year = request.GET.get("year", max_year)

        # order_by if order_by else -date
        order_by = request.GET.get("order_by", "-date")

        # filtering books
        books = self.get_books(year=year, order_by=order_by)
        years = Books.objects.get_years_of_reading(user)
        book_list_rating_by_user = get_book_list_rating_by_user(user)

        kwargs.update(
            {
                "form": self.get_form(),
                "books": books,
                "years": years,
                "current_year": year,
                "book_list_rating_by_user": book_list_rating_by_user,
            }
        )

        return super().get(request, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.get_form()

        if form.is_valid():
            new_form = form.save(commit=False)
            new_form.user = request.user
            new_form.save()
            success_message = _("You book was successful added.")
            messages.success(request, success_message)
            return HttpResponseRedirect(reverse("books") + "?signal=True")

        kwargs.update({"form": form})
        return super().get(request, *args, **kwargs)


class BookDeleteView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Books
    success_message = _("You book was successful deleted.")
    success_url = reverse_lazy("books")

    def get(self, request, *args, **kwargs):
        """
        Overwrite get because we make deletion by link on books page and
        we don't need confirmation page for deletion.
        """
        return self.post(request, *args, **kwargs)


class BookUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    form_class = BookForms
    model = Books
    template_name = "books/book-edit.html"
    success_message = _("You book was successful changed.")
    success_url = reverse_lazy("books")
