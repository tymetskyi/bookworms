from django.urls import path

from .views import BookView, BookDeleteView, BookUpdateView

urlpatterns = [
    path("", BookView.as_view(), name="books"),
    path("<int:pk>/delete/", BookDeleteView.as_view(), name="book-delete"),
    path("<int:pk>/edit/", BookUpdateView.as_view(), name="book-edit"),
]
